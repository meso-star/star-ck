/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200809L /* mmap support */
#define _DEFAULT_SOURCE 1 /* MAP_POPULATE support */
#define _BSD_SOURCE 1 /* MAP_POPULATE for glibc < 2.19 */

#include "sck.h"
#include "sck_c.h"
#include "sck_log.h"

#include <rsys/algorithm.h>
#include <rsys/cstr.h>

#include <unistd.h> /* sysconf support */

#include <errno.h>
#include <string.h>
#include <sys/mman.h> /* mmap */
#include <sys/stat.h> /* fstat */

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE int
is_stdin(FILE* stream)
{
  struct stat stream_buf;
  struct stat stdin_buf;
  ASSERT(stream);
  CHK(fstat(fileno(stream), &stream_buf) == 0);
  CHK(fstat(STDIN_FILENO, &stdin_buf) == 0);
  return stream_buf.st_dev == stdin_buf.st_dev;
}

static INLINE res_T
check_sck_create_args(const struct sck_create_args* args)
{
  /* Nothing to check. Only return RES_BAD_ARG if args is NULL */
  return args ? RES_OK : RES_BAD_ARG;
}

static INLINE res_T
check_sck_load_args(const struct sck_load_args* args)
{
  if(!args || !args->path) return RES_BAD_ARG;
  return RES_OK;
}

static INLINE res_T
check_sck_load_stream_args
  (struct sck* sck,
   const struct sck_load_stream_args* args)
{
  if(!args || !args->stream || !args->name) return RES_BAD_ARG;
  if(args->memory_mapping && is_stdin(args->stream)) {
    log_err(sck, "%s: unable to use memory mapping on data loaded from stdin\n",
      args->name);
    return RES_BAD_ARG;
  }
  return RES_OK;
}

static void
reset_sck(struct sck* sck)
{
  ASSERT(sck);
  sck->pagesize = 0;
  sck->nnodes = 0;
  darray_band_purge(&sck->bands);
  str_clear(&sck->name);
}

static INLINE int
cmp_dbl(const void* a, const void* b)
{
  const double d0 = *((const double*)a);
  const double d1 = *((const double*)b);
  return d0 < d1 ? -1 : (d0 > d1 ? 1 : 0);
}

static res_T
read_quad_pt
  (struct sck* sck,
   struct quad_pt* quad_pt,
   FILE* stream,
   size_t iband,
   size_t iquad_pt)
{
  res_T res = RES_OK;
  ASSERT(sck && quad_pt);

  quad_pt->band = darray_band_data_get(&sck->bands) + iband;

  if(fread(&quad_pt->weight, sizeof(quad_pt->weight), 1, stream) != 1) {
    log_err(sck,
      "%s: band %lu: quadrature point %lu: could not read the weight.\n",
      sck_get_name(sck), iband, iquad_pt);
    res = RES_IO_ERR;
    goto error;
  }

  if(quad_pt->weight < 0) {
    log_err(sck,
      "%s: band %lu: quadrature point %lu: invalid weight %g.\n",
      sck_get_name(sck), iband, iquad_pt, quad_pt->weight);
    res = RES_BAD_ARG;
    goto error;

  }

  quad_pt->ka_list = NULL;

exit:
  return res;
error:
  goto exit;
}

static res_T
read_band
  (struct sck* sck,
   struct band* band,
   FILE* stream)
{
  double cumul;
  size_t iquad_pt;
  size_t iband;
  uint64_t nquad_pts;
  res_T res = RES_OK;
  ASSERT(sck && band);

  band->sck = sck;
  iband = (size_t)(band - darray_band_cdata_get(&sck->bands));

  /* Read band definition */
  #define READ(Var,  Name) {                                                   \
    if(fread((Var), sizeof(*(Var)), 1, stream) != 1) {                         \
      log_err(sck, "%s: band %lu: could not read the %s.\n",                   \
        sck_get_name(sck), (unsigned long)iband, (Name));                      \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(&band->low, "band lower bound");
  READ(&band->upp, "band upper bound");
  READ(&nquad_pts, "#quadrature points");
  #undef READ

  /* Check band description */
  if(band->low < 0 || band->low > band->upp) {
    log_err(sck,
      "%s: band %lu: invalid band range [%g, %g].\n",
      sck_get_name(sck), (unsigned long)iband, band->low, band->upp);
    res = RES_BAD_ARG;
    goto error;
  }
  if(nquad_pts == 0) {
    log_err(sck,
      "%s: band %lu: invalid number fo quadrature points (#points=%lu).\n",
      sck_get_name(sck), (unsigned long)iband, (unsigned long)nquad_pts);
    res = RES_BAD_ARG;
    goto error;
  }

  /* Allocate the list of quadrature points */
  res = darray_quad_pt_resize(&band->quad_pts, nquad_pts);
  if(res != RES_OK) {
    log_err(sck,
      "%s: band %lu: could not allocate the list of quadrature points "
      "(#points=%lu).\n",
      sck_get_name(sck), (unsigned long)iband, (unsigned long)nquad_pts);
    goto error;
  }

  /* Allocate the cumulative used to sample the quadrature points */
  res = darray_double_resize(&band->quad_pts_cumul, nquad_pts);
  if(res != RES_OK) {
    log_err(sck,
      "%s: band %lu: could not allocate the cumulative of quadrature points "
      "(#points=%lu).\n",
      sck_get_name(sck), (unsigned long)iband, (unsigned long)nquad_pts);
    goto error;
  }

  /* Read the data of the quadrature points of the band */
  cumul = 0;
  FOR_EACH(iquad_pt, 0, nquad_pts) {
    struct quad_pt* quad_pt = darray_quad_pt_data_get(&band->quad_pts)+iquad_pt;

    /* Read current quadrature point */
    res = read_quad_pt(sck, quad_pt, stream, iband, iquad_pt);
    if(res != RES_OK) goto error;

    /* Compute the quadrature point cumulative */
    cumul += quad_pt->weight;
    darray_double_data_get(&band->quad_pts_cumul)[iquad_pt] = cumul;
  }

  /* The weights are not normalized */
  if(!eq_eps(cumul, 1.0, 1.e-6)) {
    log_warn(sck,
      "%s: band %lu: the weights of the quadrature points are not normalised.\n",
      sck_get_name(sck), (unsigned long)iband);

    /* Renormalize the cumulative */
    FOR_EACH(iquad_pt, 0, nquad_pts) {
      darray_double_data_get(&band->quad_pts_cumul)[iquad_pt] /= cumul;
    }
  }

  /* Handle imprecision issue */
  darray_double_data_get(&band->quad_pts_cumul)[nquad_pts-1] = 1.0;

exit:
  return res;
error:
  goto exit;
}

static res_T
map_data
  (struct sck* sck,
   const int fd, /* File descriptor */
   const size_t filesz, /* Overall filesize */
   const char* data_name,
   const off_t offset, /* Offset of the data into file */
   const size_t map_len,
   void** out_map) /* Lenght of the data to map */
{
  void* map = NULL;
  res_T res = RES_OK;
  ASSERT(sck && filesz && data_name && map_len && out_map);
  ASSERT(IS_ALIGNED((size_t)offset, (size_t)sck->pagesize));

  if((size_t)offset + map_len > filesz) {
    log_err(sck, "%s: the %s to map exceed the file size\n",
      sck_get_name(sck), data_name);
    res = RES_IO_ERR;
    goto error;
  }

  map = mmap(NULL, map_len, PROT_READ, MAP_PRIVATE|MAP_POPULATE, fd, offset);
  if(map == MAP_FAILED) {
    log_err(sck, "%s: could not map the %s -- %s\n",
      sck_get_name(sck), data_name, strerror(errno));
    res = RES_IO_ERR;
    goto error;
  }

exit:
  *out_map = map;
  return res;
error:
  if(map == MAP_FAILED) map = NULL;
  goto exit;
}

static res_T
map_file(struct sck* sck, FILE* stream)
{
  size_t filesz;
  size_t map_len;
  size_t iband;
  off_t offset;
  res_T res = RES_OK;
  ASSERT(sck && stream);

  /* Compute the length in bytes of the k to map for each band/quadrature point */
  map_len = ALIGN_SIZE(sck->nnodes * sizeof(float), sck->pagesize);

  /* Compute the offset toward the 1st list of radiative coefficients */
  offset = ftell(stream);
  offset = (off_t)ALIGN_SIZE((uint64_t)offset, sck->pagesize);

  /* Retrieve the overall filesize */
  fseek(stream, 0, SEEK_END);
  filesz = (size_t)ftell(stream);

  FOR_EACH(iband, 0, darray_band_size_get(&sck->bands)) {
    struct band* band = NULL;
    size_t iquad_pt;

    band = darray_band_data_get(&sck->bands) + iband;
    band->map_len = map_len;

    /* Mapping per band scattering coefficients */
    res = map_data(sck, fileno(stream), filesz, "scattering coefficients",
      offset, band->map_len, (void**)&band->ks_list);
    if(res != RES_OK) {
      log_err(sck,
          "%s: data mapping error for band %lu\n",
          sck_get_name(sck), (unsigned long)iband);
      goto error;
    }

    offset = (off_t)((size_t)offset + map_len);

    FOR_EACH(iquad_pt, 0, darray_quad_pt_size_get(&band->quad_pts)) {
      struct quad_pt* quad_pt = NULL;

      /* Mapping absorption coefficients per band and quadrature point */
      quad_pt = darray_quad_pt_data_get(&band->quad_pts)+iquad_pt;
      quad_pt->map_len = map_len;

      res = map_data(sck, fileno(stream), filesz, "correlated Ka",
        offset, quad_pt->map_len, (void**)&quad_pt->ka_list);
      if(res != RES_OK) {
        log_err(sck,
          "%s: data mapping error for band %lu quadrature point %lu\n",
          sck_get_name(sck), (unsigned long)iband, (unsigned long)iquad_pt);
        goto error;
      }

      offset = (off_t)((size_t)offset + map_len);
    }
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
read_padding(FILE* stream, const size_t padding)
{
  char chunk[1024];
  size_t remaining_nbytes = padding;

  while(remaining_nbytes) {
    const size_t nbytes = MMIN(sizeof(chunk), remaining_nbytes);
    if(fread(chunk, 1, nbytes, stream) != nbytes) return RES_IO_ERR;
    remaining_nbytes -= nbytes;
  }
  return RES_OK;
}

/* Return the size in bytes of the data layout and band descriptors */
static size_t
compute_sizeof_header(struct sck* sck)
{
  size_t sizeof_header = 0;
  size_t iband = 0;
  size_t nbands = 0;
  ASSERT(sck);

  sizeof_header =
    sizeof(uint64_t) /* pagesize */
  + sizeof(uint64_t) /* #bands */
  + sizeof(uint64_t);/* #nodes */

  nbands = darray_band_size_get(&sck->bands);
  FOR_EACH(iband, 0, nbands) {
    const struct band* band = darray_band_cdata_get(&sck->bands)+iband;
    const size_t nquad_pts = darray_quad_pt_size_get(&band->quad_pts);
    const size_t sizeof_band_desc =
      sizeof(double) /* Lower bound */
    + sizeof(double) /* Upper bound */
    + sizeof(uint64_t) /* #quad_pts */
    + sizeof(double) * nquad_pts; /* Weights */

    sizeof_header += sizeof_band_desc;
  }
  return sizeof_header;
}

static res_T
load_data
  (struct sck* sck,
   FILE* stream,
   const char* data_name,
   float** out_data)
{
  float* data = NULL;
  res_T res = RES_OK;
  ASSERT(sck && stream && data_name && out_data);

  data = MEM_ALLOC(sck->allocator, sizeof(float)*sck->nnodes);
  if(!data) {
    res = RES_MEM_ERR;
    log_err(sck, "%s: could not allocate the %s -- %s\n",
      sck_get_name(sck), data_name, res_to_cstr(res));
    goto error;
  }

  if(fread(data, sizeof(float), sck->nnodes, stream) != sck->nnodes) {
    res = RES_IO_ERR;
    log_err(sck, "%s: could not read the %s -- %s\n",
      sck_get_name(sck), data_name, res_to_cstr(res));
    goto error;
  }

exit:
  *out_data = data;
  return res;
error:
  if(data) { MEM_RM(sck->allocator, data); data = NULL; }
  goto exit;
}

static res_T
load_file(struct sck* sck, FILE* stream)
{
  size_t padding_bytes;
  size_t sizeof_header;
  size_t sizeof_k_list;
  size_t iband;
  size_t nbands;
  res_T res = RES_OK;

  sizeof_header = compute_sizeof_header(sck);
  sizeof_k_list = sizeof(float)*sck->nnodes;

  padding_bytes = ALIGN_SIZE(sizeof_header, sck->pagesize) - sizeof_header;
  if((res = read_padding(stream, padding_bytes)) != RES_OK) goto error;

  /* Calculate the padding between the lists of radiative coefficients. Note
   * that this padding is the same between each list */
  padding_bytes = ALIGN_SIZE(sizeof_k_list, sck->pagesize) - sizeof_k_list;

  nbands = darray_band_size_get(&sck->bands);
  FOR_EACH(iband, 0, nbands) {
    struct band* band = NULL;
    size_t iquad_pt = 0;

    band = darray_band_data_get(&sck->bands) + iband;
    ASSERT(!band->ks_list && band->sck == sck);

    /* Loading per band scattering coefficients */
    res = load_data(sck, stream, "scattering coefficients", &band->ks_list);
    if(res != RES_OK) {
      log_err(sck,
        "%s: data loading error for band %lu\n",
        sck_get_name(sck), (unsigned long)iband);
      goto error;
    }

    if((res = read_padding(stream, padding_bytes)) != RES_OK) goto error;

    FOR_EACH(iquad_pt, 0, darray_quad_pt_size_get(&band->quad_pts)) {
      struct quad_pt* quad_pt = NULL;

      quad_pt = darray_quad_pt_data_get(&band->quad_pts) + iquad_pt;

      /* Loading absorption coefficients per band and quadrature point */
      res = load_data(sck, stream, "correlated Ka", &quad_pt->ka_list);
      if(res != RES_OK) {
        log_err(sck,
          "%s: data loading error for band %lu quadrature point %lu\n",
          sck_get_name(sck), (unsigned long)iband, (unsigned long)iquad_pt);
        goto error;
      }

      if((res = read_padding(stream, padding_bytes)) != RES_OK) goto error;
    }
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
load_stream(struct sck* sck, const struct sck_load_stream_args* args)
{
  size_t iband;
  uint64_t nbands;
  res_T res = RES_OK;
  ASSERT(sck && check_sck_load_stream_args(sck, args) == RES_OK);

  reset_sck(sck);

  res = str_set(&sck->name, args->name);
  if(res != RES_OK) {
    log_err(sck, "%s: unable to duplicate path to loaded data or stream name\n",
      args->name);
    goto error;
  }

  /* Read file header */
  if(fread(&sck->pagesize, sizeof(sck->pagesize), 1, args->stream) != 1) {
    if(ferror(args->stream)) {
      log_err(sck, "%s: could not read the pagesize.\n", sck_get_name(sck));
    }
    res = RES_IO_ERR;
    goto error;
  }
  #define READ(Var, Name) {                                                    \
    if(fread((Var), sizeof(*(Var)), 1, args->stream) != 1) {                   \
      log_err(sck, "%s: could not read the %s.\n", sck_get_name(sck), (Name)); \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(&nbands, "number of bands");
  READ(&sck->nnodes, "number of nodes");
  #undef READ

  /* Check band description */
  if(!IS_ALIGNED(sck->pagesize, sck->pagesize_os)) {
    log_err(sck,
      "%s: invalid page size %lu. The page size attribute must be aligned on "
      "the page size of the operating system (%lu).\n",
      sck_get_name(sck), sck->pagesize, (unsigned long)sck->pagesize_os);
    res = RES_BAD_ARG;
    goto error;
  }

  if(!nbands) {
    log_err(sck, "%s: invalid number of bands %lu.\n",
      sck_get_name(sck), (unsigned long)nbands);
    res = RES_BAD_ARG;
    goto error;
  }
  if(!sck->nnodes) {
    log_err(sck, "%s: invalid number of nodes %lu.\n",
      sck_get_name(sck), (unsigned long)sck->nnodes);
    res = RES_BAD_ARG;
    goto error;
  }

  /* Allocate the bands */
  res = darray_band_resize(&sck->bands, nbands);
  if(res != RES_OK) {
    log_err(sck, "%s: could not allocate the list of bands (#bands=%lu).\n",
      sck_get_name(sck), (unsigned long)nbands);
    goto error;
  }

  /* Read the band description */
  FOR_EACH(iband, 0, nbands) {
    struct band* band = darray_band_data_get(&sck->bands) + iband;
    res = read_band(sck, band, args->stream);
    if(res != RES_OK) goto error;
    if(iband > 0 && band[0].low < band[-1].upp) {
      log_err(sck,
        "%s: bands must be sorted in ascending order and must not "
        "overlap (band %lu in [%g, %g[ nm; band %lu in [%g, %g[ nm).\n",
        sck_get_name(sck),
        (unsigned long)(iband-1), band[-1].low, band[-1].upp,
        (unsigned long)(iband),   band[ 0].low, band[ 0].upp);
      res = RES_BAD_ARG;
      goto error;
    }
  }

  if(args->memory_mapping) {
    res = map_file(sck, args->stream);
    if(res != RES_OK) goto error;
  } else {
    res = load_file(sck, args->stream);
    if(res != RES_OK) goto error;
  }

exit:
  return res;
error:
  reset_sck(sck);
  goto exit;
}

static INLINE int
cmp_band(const void* key, const void* item)
{
  const struct band* band = item;
  double wnum;
  ASSERT(key && item);
  wnum = *(double*)key;

  if(wnum < band->low) {
    return -1;
  } else if(wnum >= band->upp) {
    return +1;
  } else {
    return 0;
  }
}

static INLINE void
hash_quad_pt
  (struct sha256_ctx* ctx,
   const struct sck_quad_pt* pt,
   const size_t nnodes)
{
  ASSERT(ctx && pt);
  #define HASH(Var, Nb) sha256_ctx_update(ctx, (const char*)(Var), sizeof(*Var)*(Nb))
  HASH(&pt->weight, 1);
  HASH(&pt->id, 1);
  HASH(pt->ka_list, nnodes);
  #undef HASH
}

static INLINE void
hash_band(struct sha256_ctx* ctx, const struct sck_band* band)
{
  size_t iquad_pt;
  ASSERT(ctx && band);

  #define HASH(Var, Nb) sha256_ctx_update(ctx, (const char*)(Var), sizeof(*Var)*(Nb))
  HASH(&band->lower, 1);
  HASH(&band->upper, 1);
  HASH(&band->quad_pts_count, 1);
  HASH(band->ks_list, band->sck__->nnodes);
  #undef HASH

  FOR_EACH(iquad_pt, 0, band->quad_pts_count) {
    struct sck_quad_pt quad_pt;
    SCK(band_get_quad_pt(band, iquad_pt, &quad_pt));
    hash_quad_pt(ctx, &quad_pt, band->sck__->nnodes);
  }
}

static void
release_sck(ref_T* ref)
{
  struct sck* sck;
  ASSERT(ref);
  sck = CONTAINER_OF(ref, struct sck, ref);
  if(sck->logger == &sck->logger__) logger_release(&sck->logger__);
  darray_band_release(&sck->bands);
  str_release(&sck->name);
  MEM_RM(sck->allocator, sck);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
sck_create
  (const struct sck_create_args* args,
   struct sck** out_sck)
{
  struct sck* sck = NULL;
  struct mem_allocator* allocator = NULL;
  res_T res = RES_OK;

  if(!out_sck) { res = RES_BAD_ARG; goto error; }
  res = check_sck_create_args(args);
  if(res != RES_OK) goto error;

  allocator = args->allocator ? args->allocator : &mem_default_allocator;
  sck = MEM_CALLOC(allocator, 1, sizeof(*sck));
  if(!sck) {
    if(args->verbose) {
      #define ERR_STR "Could not allocate the Star-CK device.\n"
      if(args->logger) {
        logger_print(args->logger, LOG_ERROR, ERR_STR);
      } else {
        fprintf(stderr, MSG_ERROR_PREFIX ERR_STR);
      }
      #undef ERR_STR
    }
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&sck->ref);
  sck->allocator = allocator;
  sck->verbose = args->verbose;
  sck->pagesize_os = (size_t)sysconf(_SC_PAGESIZE);
  str_init(allocator, &sck->name);
  darray_band_init(allocator, &sck->bands);
  if(args->logger) {
    sck->logger = args->logger;
  } else {
    setup_log_default(sck);
  }

exit:
  if(out_sck) *out_sck = sck;
  return res;
error:
  if(sck) {
    SCK(ref_put(sck));
    sck = NULL;
  }
  goto exit;
}

res_T
sck_ref_get(struct sck* sck)
{
  if(!sck) return RES_BAD_ARG;
  ref_get(&sck->ref);
  return RES_OK;
}

res_T
sck_ref_put(struct sck* sck)
{
  if(!sck) return RES_BAD_ARG;
  ref_put(&sck->ref, release_sck);
  return RES_OK;
}

res_T
sck_load(struct sck* sck, const struct sck_load_args* args)
{
  struct sck_load_stream_args stream_args = SCK_LOAD_STREAM_ARGS_NULL;
  FILE* file = NULL;
  res_T res = RES_OK;

  if(!sck) { res = RES_BAD_ARG; goto error; }
  res = check_sck_load_args(args);
  if(res != RES_OK) goto error;

  file = fopen(args->path, "r");
  if(!file) {
    log_err(sck, "%s: error opening file `%s'.\n", FUNC_NAME, args->path);
    res = RES_IO_ERR;
    goto error;
  }

  stream_args.stream = file;
  stream_args.name = args->path;
  stream_args.memory_mapping = args->memory_mapping;
  res = load_stream(sck, &stream_args);
  if(res != RES_OK) goto error;

exit:
  if(file) fclose(file);
  return res;
error:
  goto exit;
}

res_T
sck_load_stream(struct sck* sck, const struct sck_load_stream_args* args)
{
  res_T res = RES_OK;
  if(!sck) return RES_BAD_ARG;
  res = check_sck_load_stream_args(sck, args);
  if(res != RES_OK) return res;
  return load_stream(sck, args);
}

res_T
sck_validate(const struct sck* sck)
{
  size_t iband;
  if(!sck) return RES_BAD_ARG;

  FOR_EACH(iband, 0, sck_get_bands_count(sck)) {
    struct sck_band band = SCK_BAND_NULL;
    size_t inode;
    size_t iquad_pt;

    SCK(get_band(sck, iband, &band));

    /* Check band limits */
    if(band.lower != band.lower /* NaN? */
    || band.upper != band.upper) { /* NaN? */
      log_err(sck, "%s: invalid limits for band %lu: [%g, %g[\n",
        sck_get_name(sck), (unsigned long)iband, band.lower, band.upper);
      return RES_BAD_ARG;
    }

    /* Check scattering coefficients */
    FOR_EACH(inode, 0, sck_get_nodes_count(sck)) {
      if(band.ks_list[inode] != band.ks_list[inode] /* NaN? */
      || band.ks_list[inode] < 0) {
        log_err(sck,
          "%s: invalid scattering coefficient for band %lu at node %lu: %g\n",
          sck_get_name(sck), (unsigned long)iband, (unsigned long) inode,
          band.ks_list[inode]);
        return RES_BAD_ARG;
      }
    }

    FOR_EACH(iquad_pt, 0, band.quad_pts_count) {
      struct sck_quad_pt quad_pt = SCK_QUAD_PT_NULL;

      SCK(band_get_quad_pt(&band, iquad_pt, &quad_pt));

      /* Check quadrature point weight */
      if(quad_pt.weight != quad_pt.weight /* NaN? */
      || quad_pt.weight <= 0) {
        log_err(sck,
          "%s: invalid weight for quadrature point %lu of band %lu: %g\n",
          sck_get_name(sck), (unsigned long)iquad_pt, (unsigned long)iband,
          quad_pt.weight);
        return RES_BAD_ARG;
      }

      /* Check absorption coefficient */
      FOR_EACH(inode, 0, sck_get_nodes_count(sck)) {
        if(quad_pt.ka_list[inode] != quad_pt.ka_list[inode] /* NaN? */
        || quad_pt.ka_list[inode] < 0) {
          log_err(sck,
            "%s: invalid absorption coefficient for quadrature point %lu "
            "of band %lu at node %lu: %g\n",
            sck_get_name(sck), (unsigned long)iquad_pt, (unsigned long)iband,
            (unsigned long)inode, quad_pt.ka_list[inode]);
          return RES_BAD_ARG;
        }
      }
    }
  }
  return RES_OK;
}

size_t
sck_get_bands_count(const struct sck* sck)
{
  ASSERT(sck);
  return darray_band_size_get(&sck->bands);
}

size_t
sck_get_nodes_count(const struct sck* sck)
{
  ASSERT(sck);
  return sck->nnodes;
}

res_T
sck_get_band
  (const struct sck* sck,
   const size_t iband,
   struct sck_band* sck_band)
{
  const struct band* band = NULL;
  res_T res = RES_OK;

  if(!sck || !sck_band) {
    res = RES_BAD_ARG;
    goto error;
  }

  if(iband >= sck_get_bands_count(sck)) {
    log_err(sck, "%s: invalid band index %lu.\n",
      FUNC_NAME, (unsigned long)iband);
    res = RES_BAD_ARG;
    goto error;
  }

  band = darray_band_cdata_get(&sck->bands) + iband;
  sck_band->lower = band->low;
  sck_band->upper = band->upp;
  sck_band->ks_list = band->ks_list;
  sck_band->quad_pts_count = darray_quad_pt_size_get(&band->quad_pts);
  sck_band->id = iband;
  sck_band->sck__ = sck;
  sck_band->band__ = band;

exit:
  return res;
error:
  goto exit;
}

res_T
sck_find_bands
  (const struct sck* sck,
   const double range[2],
   size_t ibands[2])
{
  const struct band* bands = NULL;
  const struct band* low = NULL;
  const struct band* upp = NULL;
  size_t nbands = 0;
  res_T res = RES_OK;

  if(!sck || !range || !ibands || range[0] > range[1]) {
    res = RES_BAD_ARG;
    goto error;
  }

  bands = darray_band_cdata_get(&sck->bands);
  nbands = darray_band_size_get(&sck->bands);

  low = search_lower_bound(range+0, bands, nbands, sizeof(*bands), cmp_band);
  if(low) {
    ibands[0] = (size_t)(low - bands);
  } else {
    /* The submitted range does not overlap any band */
    ibands[0] = SIZE_MAX;
    ibands[1] = 0;
    goto exit;
  }

  if(range[0] == range[1]) { /* No more to search */
    if(range[0] <  low->low) {
      /* The wavelength is not included in any band */
      ibands[0] = SIZE_MAX;
      ibands[1] = 0;
    } else {
      ASSERT(range[0] < low->upp);
      ibands[1] = ibands[0];
    }
    goto exit;
  }

  upp = search_lower_bound(range+1, bands, nbands, sizeof(*bands), cmp_band);

  /* The submitted range overlaps the remaining bands */
  if(!upp) {
    ibands[1] = nbands - 1;

  /* The upper band includes range[1] */
  } else if(upp->low <= range[1]) {
    ibands[1] = (size_t)(upp - bands);

  /* The upper band is greater than range[1] and therefre must be rejected */
  } else if(upp->low > range[1]) {
    if(upp != bands) {
      ibands[1] = (size_t)(upp - bands - 1);
    } else {
      ibands[0] = SIZE_MAX;
      ibands[1] = 0;
    }
  }

exit:
  return res;
error:
  goto exit;
}

res_T
sck_band_get_quad_pt
  (const struct sck_band* sck_band,
   const size_t iquad_pt,
   struct sck_quad_pt* sck_quad_pt)
{
  const struct band* band = NULL;
  const struct quad_pt* quad_pt = NULL;
  res_T res = RES_OK;

  if(!sck_band || !sck_quad_pt) {
    res = RES_BAD_ARG;
    goto error;
  }

  band = sck_band->band__;
  if(iquad_pt >= sck_band->quad_pts_count) {
    log_err(sck_band->sck__,
      "%s: band %lu: invalid quadrature point index %lu.\n",
      FUNC_NAME, (unsigned long)sck_band->id, (unsigned long)iquad_pt);
    res = RES_BAD_ARG;
    goto error;
  }

  quad_pt = darray_quad_pt_cdata_get(&band->quad_pts) + iquad_pt;
  sck_quad_pt->ka_list = quad_pt->ka_list;
  sck_quad_pt->weight = quad_pt->weight;
  sck_quad_pt->id = iquad_pt;

exit:
  return res;
error:
  goto exit;
}

res_T
sck_band_sample_quad_pt
  (const struct sck_band* sck_band,
   const double r, /* Canonical random number in [0, 1[ */
   size_t* iquad_pt)
{
  const struct band* band = NULL;
  const double* cumul = NULL;
  const double* find = NULL;
  double r_next = nextafter(r, DBL_MAX);
  size_t i;

  if(!sck_band || !iquad_pt) return RES_BAD_ARG;

  if(r < 0 || r >= 1) {
    log_err(sck_band->sck__,
      "%s: invalid canonical random number `%g'.\n", FUNC_NAME, r);
    return RES_BAD_ARG;
  }

  band = sck_band->band__;
  cumul = darray_double_cdata_get(&band->quad_pts_cumul);

  /* Use r_next rather than r in order to find the first entry that is not less
   * than *or equal* to r */
  find = search_lower_bound(&r_next, cumul, sck_band->quad_pts_count,
    sizeof(double), cmp_dbl);
  ASSERT(find);

  /* Compute the index of the found quadrature point */
  i = (size_t)(find - cumul);
  ASSERT(i < sck_band->quad_pts_count);
  ASSERT(cumul[i] > r);
  ASSERT(!i || cumul[i-1] <= r);

  *iquad_pt = i;

  return RES_OK;
}

res_T
sck_quad_pt_compute_hash
  (const struct sck_band* band,
   const size_t iquad_pt,
   hash256_T hash)
{
  struct sha256_ctx ctx;
  struct sck_quad_pt quad_pt;
  res_T res = RES_OK;

  if(!band || !hash) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = sck_band_get_quad_pt(band, iquad_pt, &quad_pt);
  if(res != RES_OK) goto error;

  sha256_ctx_init(&ctx);
  hash_quad_pt(&ctx, &quad_pt, band->sck__->nnodes);
  sha256_ctx_finalize(&ctx, hash);

exit:
  return res;
error:
  goto exit;
}

res_T
sck_band_compute_hash
  (const struct sck* sck,
   const size_t iband,
   hash256_T hash)
{
  struct sha256_ctx ctx;
  struct sck_band band;
  res_T res = RES_OK;

  if(!sck || !hash) {
    res =  RES_BAD_ARG;
    goto error;
  }

  res = sck_get_band(sck, iband, &band);
  if(res != RES_OK) goto error;

  sha256_ctx_init(&ctx);
  hash_band(&ctx, &band);
  sha256_ctx_finalize(&ctx, hash);

exit:
  return res;
error:
  goto exit;
}

res_T
sck_compute_hash(const struct sck* sck, hash256_T hash)
{
  struct sha256_ctx ctx;
  size_t iband;
  res_T res = RES_OK;

  if(!sck || !hash) {
    res = RES_BAD_ARG;
    goto error;
  }

  sha256_ctx_init(&ctx);
  sha256_ctx_update(&ctx, (const char*)&sck->pagesize, sizeof(sck->pagesize));
  sha256_ctx_update(&ctx, (const char*)&sck->nnodes, sizeof(sck->nnodes));
  FOR_EACH(iband, 0, darray_band_size_get(&sck->bands)) {
    struct sck_band band;
    SCK(get_band(sck, iband, &band));
    hash_band(&ctx, &band);
  }
  sha256_ctx_finalize(&ctx, hash);

exit:
  return res;
error:
  goto exit;
}

const char*
sck_get_name(const struct sck* sck)
{
  ASSERT(sck);
  return str_cget(&sck->name);
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
void
band_release(struct band* band)
{
  ASSERT(band);
  darray_quad_pt_release(&band->quad_pts);
  darray_double_release(&band->quad_pts_cumul);

  if(!band->ks_list) return;

  if(!band->map_len) {
    MEM_RM(band->sck->allocator, band->ks_list);
  } else if(band->ks_list != MAP_FAILED) {
    munmap(band->ks_list, band->map_len);
  }
}

res_T
band_copy(struct band* dst, const struct band* src)
{
  res_T res = RES_OK;
  ASSERT(dst && src);

  dst->sck = src->sck;
  dst->low = src->low;
  dst->upp = src->upp;

  dst->map_len = src->map_len;
  dst->ks_list = NULL;

  if(src->map_len) {
    /* The ks are mapped: copy the pointer */
    dst->ks_list = src->ks_list;

  } else if(src->ks_list != NULL) {
    /* The ks are loaded: duplicate table contents */
    const size_t memsz = sizeof(*dst->ks_list)*src->sck->nnodes;
    dst->ks_list = MEM_ALLOC(dst->sck->allocator, memsz);
    if(!dst->ks_list) return RES_MEM_ERR;
    memcpy(dst->ks_list, src->ks_list, memsz);
  }

  res = darray_quad_pt_copy(&dst->quad_pts, &src->quad_pts);
  if(res != RES_OK) return res;
  res = darray_double_copy(&dst->quad_pts_cumul, &src->quad_pts_cumul);
  if(res != RES_OK) return res;
  return RES_OK;
}

void
quad_pt_release(struct quad_pt* quad_pt)
{
  ASSERT(quad_pt);
  if(!quad_pt->ka_list) return;

  if(!quad_pt->map_len) {
    MEM_RM(quad_pt->band->sck->allocator, quad_pt->ka_list);
  } else if(quad_pt->ka_list != MAP_FAILED) {
    munmap(quad_pt->ka_list, quad_pt->map_len);
  }
}

res_T
quad_pt_copy(struct quad_pt* dst, const struct quad_pt* src)
{
  ASSERT(dst && src);

  dst->band = src->band;
  dst->map_len = src->map_len;
  dst->weight = src->weight;
  dst->ka_list = NULL;
  if(src->map_len) {
    /* The ka are mapped: copy the pointer */
    dst->ka_list = src->ka_list;

  } else if(src->ka_list != NULL) {
    /* The ka are loaded: duplicate table contents */
    const size_t memsz = sizeof(*dst->ka_list)*src->band->sck->nnodes;
    dst->ka_list = MEM_ALLOC(dst->band->sck->allocator, memsz);
    if(!dst->ka_list) return RES_MEM_ERR;
    memcpy(dst->ka_list, src->ka_list, memsz);
  }
  return RES_OK;
}
