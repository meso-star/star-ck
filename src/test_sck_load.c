/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* nextarfter */

#include "sck.h"

#include <rsys/math.h>
#include <rsys/mem_allocator.h>
#include <math.h>
#include <string.h>

static INLINE double
rand_canonic(void)
{
  int r;
  while((r = rand()) == RAND_MAX);
  return (double)r / (double)RAND_MAX;
}

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
check_sck_load
  (const struct sck* sck,
   const size_t nbands,
   const size_t nnodes)
{
  #define NMOMENTS 4
  struct sck_band band = SCK_BAND_NULL;
  struct sck_quad_pt qpt = SCK_QUAD_PT_NULL;
  size_t iband;
  size_t iqpt;
  const size_t nsamps = 1000000;
  size_t isamp;

  CHK(sck);
  CHK(nbands);
  CHK(nnodes);

  CHK(sck_validate(NULL) == RES_BAD_ARG);
  CHK(sck_validate(sck) == RES_OK);

  CHK(sck_get_bands_count(sck) == nbands);
  CHK(sck_get_nodes_count(sck) == nnodes);

  CHK(sck_get_band(NULL, 0, &band) == RES_BAD_ARG);
  CHK(sck_get_band(sck, nbands, &band) == RES_BAD_ARG);
  CHK(sck_get_band(sck, nbands, NULL) == RES_BAD_ARG);
  CHK(sck_get_band(sck, 0, &band) == RES_OK);

  CHK(band.quad_pts_count == 1);
  CHK(sck_band_get_quad_pt(NULL, 0, &qpt) == RES_BAD_ARG);
  CHK(sck_band_get_quad_pt(&band, 1, &qpt) == RES_BAD_ARG);
  CHK(sck_band_get_quad_pt(&band, 0, NULL) == RES_BAD_ARG);

  CHK(sck_band_sample_quad_pt(NULL, 0, &iqpt) == RES_BAD_ARG);
  CHK(sck_band_sample_quad_pt(&band, 1, &iqpt) == RES_BAD_ARG);
  CHK(sck_band_sample_quad_pt(&band, 0, NULL) == RES_BAD_ARG);

  FOR_EACH(iband, 0, nbands) {
    const double low = (double)(iband+1);
    const double upp = (double)(iband+2);
    const size_t nqpts = iband + 1;
    double sum[NMOMENTS] = {0};
    double sum2[NMOMENTS] = {0};
    double E[NMOMENTS] = {0};
    double V[NMOMENTS] = {0};
    double SE[NMOMENTS] = {0};
    double ref;
    size_t inode;
    int imoment;

    CHK(sck_get_band(sck, iband, &band) == RES_OK);
    CHK(band.lower == low);
    CHK(band.upper == upp);
    CHK(band.id == iband);
    CHK(band.quad_pts_count == nqpts);

    FOR_EACH(inode, 0, nnodes) {
      const float ks = (float)(iband*2000 + inode);
      CHK(band.ks_list[inode] == ks);
    }

    FOR_EACH(iqpt, 0, nqpts) {
      const double weight = 1.0/(double)nqpts;

      CHK(sck_band_get_quad_pt(&band, iqpt, &qpt) == RES_OK);
      CHK(qpt.weight == weight);
      CHK(qpt.id == iqpt);

      FOR_EACH(inode, 0, nnodes) {
        const float ka = (float)(iband*1000 + iqpt*100 + inode);
        CHK(qpt.ka_list[inode] == ka);
      }
    }

    /* Check the sampling of the quadrature points */
    FOR_EACH(isamp, 0, nsamps) {
      const double r = rand_canonic();
      double w[NMOMENTS];

      CHK(sck_band_sample_quad_pt(&band, r, &iqpt) == RES_OK);
      ASSERT(iqpt < band.quad_pts_count);
      CHK(sck_band_get_quad_pt(&band, iqpt, &qpt) == RES_OK);
      FOR_EACH(imoment, 0, NMOMENTS) {
        if(imoment == 0) {
          w[imoment] = (double)(qpt.id + 1);
        } else {
          w[imoment] = (double)(qpt.id + 1) * w[imoment-1];;
        }
        sum[imoment] += w[imoment];
        sum2[imoment] += w[imoment]*w[imoment];
      }
    }

    FOR_EACH(imoment, 0, NMOMENTS) {
      E[imoment] = sum[imoment] / (double)nsamps;
      V[imoment] = sum2[imoment] / (double)nsamps - E[imoment]*E[imoment];
      SE[imoment] = sqrt(V[imoment]/(double)nsamps);
    }

    ref = (double)(nqpts+1)/2.0;
    CHK(eq_eps(ref, E[0], 3*SE[0]));

    ref = (double)(2*nqpts*nqpts + 3*nqpts + 1) / 6.0;
    CHK(eq_eps(ref, E[1], 3*SE[1]));

    ref = ((double)nqpts * pow((double)nqpts + 1.0, 2.0)) / 4.0;
    CHK(eq_eps(ref, E[2], 3*SE[2]));

    ref = (double)1.0/30.0 * (double)
      (6*nqpts*nqpts*nqpts*nqpts + 15*nqpts*nqpts*nqpts + 10*nqpts*nqpts - 1);
    CHK(eq_eps(ref, E[3], 3*SE[3]));
  }
}

static void
write_sck
  (FILE* fp,
   const uint64_t pagesize,
   const uint64_t nbands,
   const uint64_t nnodes)
{
  uint64_t iband;
  const char byte = 0;

  /* Write the header */
  CHK(fwrite(&pagesize, sizeof(pagesize), 1, fp) == 1);
  CHK(fwrite(&nbands, sizeof(nbands), 1, fp) == 1);
  CHK(fwrite(&nnodes, sizeof(nnodes), 1, fp) == 1);

  FOR_EACH(iband, 0, nbands) {
    const double low = (double)(iband+1);
    const double upp = (double)(iband+2);
    const uint64_t nqpts = iband + 1;
    uint64_t iqpt;

    /* Write band description */
    CHK(fwrite(&low, sizeof(low), 1, fp) == 1);
    CHK(fwrite(&upp, sizeof(upp), 1, fp) == 1);
    CHK(fwrite(&nqpts, sizeof(nqpts), 1, fp) == 1);

    /* Write per band quadrature points */
    FOR_EACH(iqpt, 0, nqpts) {
      const double weight = 1.0/(double)nqpts;
      CHK(fwrite(&weight, sizeof(weight), 1, fp) == 1);
    }
  }

  /* Write per band ks and per quadrature point ka */
  FOR_EACH(iband, 0, nbands) {
    const uint64_t nqpts = iband + 1;
    uint64_t inode;
    uint64_t iqpt;
    /* Padding */
    CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize), SEEK_SET)==0);

    FOR_EACH(inode, 0, nnodes) {
      const float ks = (float)(iband*2000 + inode);
      CHK(fwrite(&ks, sizeof(ks), 1, fp) == 1);
    }

    FOR_EACH(iqpt, 0, nqpts) {

      /* Padding */
      CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize), SEEK_SET)==0);

      FOR_EACH(inode, 0, nnodes) {
        const float ka = (float)(iband*1000 + iqpt*100 + inode);
        CHK(fwrite(&ka, sizeof(ka), 1, fp) == 1);
      }
    }
  }

  /* Padding. Write one char to position the EOF indicator */
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize)-1, SEEK_SET) == 0);
  CHK(fwrite(&byte, sizeof(byte), 1, fp) == 1);
}

static void
test_load(struct sck* sck)
{
  hash256_T hash0;
  hash256_T hash1;
  hash256_T band_hash0;
  hash256_T band_hash1;
  hash256_T pt_hash0;
  hash256_T pt_hash1;
  struct sck_load_args args = SCK_LOAD_ARGS_NULL;
  struct sck_load_stream_args stream_args = SCK_LOAD_STREAM_ARGS_NULL;
  struct sck_band band;

  FILE* fp = NULL;
  const char* filename = "test_file.sck";
  const uint64_t pagesize = 16384;
  const uint64_t nbands = 11;
  const uint64_t nnodes = 1000;

  CHK(fp = fopen(filename, "w+"));
  write_sck(fp, pagesize, nbands, nnodes);
  rewind(fp);

  stream_args.stream = fp;
  stream_args.name = filename;
  CHK(sck_load_stream(NULL, &stream_args) == RES_BAD_ARG);
  CHK(sck_load_stream(sck, NULL) == RES_BAD_ARG);
  stream_args.stream = NULL;
  CHK(sck_load_stream(sck, NULL) == RES_BAD_ARG);
  stream_args.stream = fp;
  CHK(sck_load_stream(sck, &stream_args) == RES_OK);

  CHK(!strcmp(sck_get_name(sck), filename));

  CHK(sck_compute_hash(sck, NULL) == RES_BAD_ARG);
  CHK(sck_compute_hash(NULL, hash0) == RES_BAD_ARG);
  CHK(sck_compute_hash(sck, hash0) == RES_OK);

  CHK(sck_band_compute_hash(NULL, 0, band_hash0) == RES_BAD_ARG);
  CHK(sck_band_compute_hash(sck, nbands, band_hash0) == RES_BAD_ARG);
  CHK(sck_band_compute_hash(sck, 0, NULL) == RES_BAD_ARG);
  CHK(sck_band_compute_hash(sck, 0, band_hash0) == RES_OK);
  CHK(!hash256_eq(band_hash0, hash0));

  CHK(sck_get_band(sck, 0, &band) == RES_OK);
  CHK(sck_quad_pt_compute_hash(NULL, 0, pt_hash0) == RES_BAD_ARG);
  CHK(sck_quad_pt_compute_hash(&band, band.quad_pts_count, pt_hash0) == RES_BAD_ARG);
  CHK(sck_quad_pt_compute_hash(&band, 0, NULL) == RES_BAD_ARG);
  CHK(sck_quad_pt_compute_hash(&band, 0, pt_hash0) == RES_OK);
  CHK(!hash256_eq(pt_hash0, band_hash0));
  CHK(!hash256_eq(pt_hash0, hash0));

  check_sck_load(sck, nbands, nnodes);
  rewind(fp);

  stream_args.name = NULL;
  CHK(sck_load_stream(sck, &stream_args) == RES_BAD_ARG);
  stream_args.name = SCK_LOAD_STREAM_ARGS_NULL.name;
  stream_args.memory_mapping = 1;
  CHK(sck_load_stream(sck, &stream_args) == RES_OK);
  CHK(!strcmp(sck_get_name(sck), SCK_LOAD_STREAM_ARGS_NULL.name));

  args.path = "nop";
  CHK(sck_load(NULL, &args) == RES_BAD_ARG);
  CHK(sck_load(sck, NULL) == RES_BAD_ARG);
  CHK(sck_load(sck, &args) == RES_IO_ERR);
  args.path = filename;
  CHK(sck_load(sck, &args) == RES_OK);
  CHK(!strcmp(sck_get_name(sck), args.path));
  check_sck_load(sck, nbands, nnodes);

  CHK(sck_compute_hash(sck, hash1) == RES_OK);
  CHK(hash256_eq(hash0, hash1));

  rewind(fp);
  write_sck(fp, pagesize, nbands+1, nnodes);
  rewind(fp);

  stream_args.stream = fp;
  stream_args.name = filename;
  CHK(sck_load_stream(sck, &stream_args) == RES_OK);
  CHK(sck_compute_hash(sck, hash1) == RES_OK);
  CHK(!hash256_eq(hash0, hash1));

  CHK(sck_band_compute_hash(sck, 0, band_hash1) == RES_OK);
  CHK(hash256_eq(band_hash0, band_hash1));

  CHK(sck_get_band(sck, 0, &band) == RES_OK);
  CHK(sck_quad_pt_compute_hash(&band, 0, pt_hash1) == RES_OK);
  CHK(hash256_eq(pt_hash0, pt_hash1));

  CHK(fclose(fp) == 0);
}

static void
test_load_fail(struct sck* sck)
{
  struct sck_load_stream_args stream_args = SCK_LOAD_STREAM_ARGS_NULL;
  FILE* fp = NULL;
  double low;
  double upp;


  /* The pagesize is less than the operating system page size*/
  CHK(fp = tmpfile());
  write_sck(fp, 2048, 1, 1);
  rewind(fp);
  stream_args.stream = fp;
  CHK(sck_load_stream(sck, &stream_args) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* The pagesize is not a power of two */
  CHK(fp = tmpfile());
  write_sck(fp, 4100, 1, 1);
  rewind(fp);
  stream_args.stream = fp;
  CHK(sck_load_stream(sck, &stream_args) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Wrong #bands */
  CHK(fp = tmpfile());
  write_sck(fp, 4096, 0, 1);
  rewind(fp);
  stream_args.stream = fp;
  CHK(sck_load_stream(sck, &stream_args) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Wrong #nodes */
  CHK(fp = tmpfile());
  write_sck(fp, 4096, 1, 0);
  rewind(fp);
  stream_args.stream = fp;
  CHK(sck_load_stream(sck, &stream_args) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Wrong band boundaries */
  low = 1;
  upp = 0;
  CHK(fp = tmpfile());
  write_sck(fp, 4096, 1, 1);
  CHK(fseek(fp, 24, SEEK_SET) == 0);
  CHK(fwrite(&low, sizeof(low), 1, fp) == 1);
  CHK(fwrite(&upp, sizeof(upp), 1, fp) == 1);
  rewind(fp);
  stream_args.stream = fp;
  CHK(sck_load_stream(sck, &stream_args) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Unsorted bands */
  CHK(fp = tmpfile());
  write_sck(fp, 4096, 2, 1);
  CHK(fseek(fp, 24, SEEK_SET) == 0);
  low = 1; upp = 2;
  CHK(fwrite(&low, sizeof(low), 1, fp) == 1);
  CHK(fwrite(&upp, sizeof(upp), 1, fp) == 1);
  CHK(fseek(fp, 56, SEEK_SET) == 0);
  low = 0; upp = 1;
  CHK(fwrite(&low, sizeof(low), 1, fp) == 1);
  CHK(fwrite(&upp, sizeof(upp), 1, fp) == 1);
  rewind(fp);
  stream_args.stream = fp;
  CHK(sck_load_stream(sck, &stream_args) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Bands overlap */
  CHK(fp = tmpfile());
  write_sck(fp, 4096, 2, 1);
  CHK(fseek(fp, 24, SEEK_SET) == 0);
  low = 0; upp = 1;
  CHK(fwrite(&low, sizeof(low), 1, fp) == 1);
  CHK(fwrite(&upp, sizeof(upp), 1, fp) == 1);
  CHK(fseek(fp, 56, SEEK_SET) == 0);
  low = 0.5; upp = 1.5;
  CHK(fwrite(&low, sizeof(low), 1, fp) == 1);
  CHK(fwrite(&upp, sizeof(upp), 1, fp) == 1);
  rewind(fp);
  stream_args.stream = fp;
  CHK(sck_load_stream(sck, &stream_args) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);
}

static void
test_load_files(struct sck* sck, int argc, char** argv)
{
  int i;
  CHK(sck);
  FOR_EACH(i, 1, argc) {
    hash256_T hash;
    size_t nnodes;
    size_t nbands;
    size_t iband;

    if(!strcmp(argv[i], "-")) {
      struct sck_load_stream_args args = SCK_LOAD_STREAM_ARGS_NULL;
      printf("Load from stdin\n");
      args.stream = stdin;
      args.name = "stdin";
      args.memory_mapping = 1;
      CHK(sck_load_stream(sck, &args) == RES_BAD_ARG);
      args.memory_mapping = 0;
      CHK(sck_load_stream(sck, &args) == RES_OK);
    } else {
      struct sck_load_args args = SCK_LOAD_ARGS_NULL;
      printf("Load %s\n", argv[1]);
      args.path = argv[i];
      args.memory_mapping = 1;
      CHK(sck_load(sck, &args) == RES_OK);
    }
    nbands = sck_get_bands_count(sck);
    nnodes = sck_get_nodes_count(sck);
    CHK(nbands);
    CHK(nnodes);

    FOR_EACH(iband, 0, nbands) {
      struct sck_band band = SCK_BAND_NULL;

      CHK(sck_get_band(sck, iband, &band) == RES_OK);
      printf("band %lu in [%g, %g[ nm with %lu quadrature points\n",
        (unsigned long)band.id,
        band.lower, band.upper,
        (unsigned long)band.quad_pts_count);
    }

    CHK(sck_validate(sck) == RES_OK);
    CHK(sck_compute_hash(sck, hash) == RES_OK);
  }
}

static void
test_find(struct sck* sck)
{
  struct sck_load_stream_args stream_args = SCK_LOAD_STREAM_ARGS_NULL;
  size_t ibands[2];
  double range[2];
  FILE* fp;

  CHK(fp = tmpfile());
  write_sck(fp, 4096, 10, 1);
  rewind(fp);
  stream_args.stream = fp;
  stream_args.memory_mapping = 1;
  CHK(sck_load_stream(sck, &stream_args) == RES_OK);

  range[0] = 0;
  range[1] = 10;
  CHK(sck_find_bands(NULL, range, ibands) == RES_BAD_ARG);
  CHK(sck_find_bands(sck, NULL, ibands) == RES_BAD_ARG);
  CHK(sck_find_bands(sck, range, NULL) == RES_BAD_ARG);
  CHK(sck_find_bands(sck, range, ibands) == RES_OK);
  CHK(ibands[0] == 0 && ibands[1] == 9);

  range[0] = 10;
  range[1] = 0;
  CHK(sck_find_bands(sck, range, ibands) == RES_BAD_ARG);

  range[0] = 11;
  range[1] = 12;
  CHK(sck_find_bands(sck, range, ibands) == RES_OK);
  CHK(ibands[0] > ibands[1]);

  range[0] = 11;
  range[1] = 11;
  CHK(sck_find_bands(sck, range, ibands) == RES_OK);
  CHK(ibands[0] > ibands[1]);

  range[0] = 0;
  range[1] = nextafter(1, 0);
  CHK(sck_find_bands(sck, range, ibands) == RES_OK);
  CHK(ibands[0] > ibands[1]);

  range[0] = 0;
  range[1] = 0;
  CHK(sck_find_bands(sck, range, ibands) == RES_OK);
  CHK(ibands[0] > ibands[1]);

  range[0] = nextafter(1, 0);
  range[1] = nextafter(1, 0);
  CHK(sck_find_bands(sck, range, ibands) == RES_OK);
  CHK(ibands[0] > ibands[1]);

  range[0] = 2;
  range[1] = 2;
  CHK(sck_find_bands(sck, range, ibands) == RES_OK);
  CHK(ibands[0] == 1 && ibands[1] == 1);

  range[0] = 0;
  range[1] = 1;
  CHK(sck_find_bands(sck, range, ibands) == RES_OK);
  CHK(ibands[0] == 0 && ibands[1] == 0);

  range[0] = 1;
  range[1] = nextafterf(2, 1);
  CHK(sck_find_bands(sck, range, ibands) == RES_OK);
  CHK(ibands[0] == 0 && ibands[1] == 0);

  range[0] = 2;
  range[1] = 20;
  CHK(sck_find_bands(sck, range, ibands) == RES_OK);
  CHK(ibands[0] == 1 && ibands[1] == 9);

  range[0] = 1.5;
  range[1] = 2;
  CHK(sck_find_bands(sck, range, ibands) == RES_OK);
  CHK(ibands[0] == 0 && ibands[1] == 1);

  range[0] = 3.1;
  range[1] = nextafter(6, 0);
  CHK(sck_find_bands(sck, range, ibands) == RES_OK);
  CHK(ibands[0] == 2 && ibands[1] == 4);

  range[0] = 3.1;
  range[1] = 7;
  CHK(sck_find_bands(sck, range, ibands) == RES_OK);
  CHK(ibands[0] == 2 && ibands[1] == 6);

  CHK(fclose(fp) == 0);
}

/*******************************************************************************
 * Main function
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct sck_create_args args = SCK_CREATE_ARGS_DEFAULT;
  struct sck* sck = NULL;
  (void)argc, (void)argv;

  args.verbose = 1;
  CHK(sck_create(&args, &sck) == RES_OK);
  if(argc > 1) {
    test_load_files(sck, argc, argv);
  } else {
    test_load(sck);
    test_load_fail(sck);
    test_find(sck);
  }

  CHK(sck_ref_put(sck) == RES_OK);
  CHK(mem_allocated_size() == 0);
  return 0;
}
